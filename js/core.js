(function ($, Drupal, drupalSettings) {

  "use strict";

  Drupal.behaviors.ExternalMediaCore = {
    attach: function (context, settings) {

      // Hide the browser default upload elements. Replace them with custom options.
      $('.form-type-external-media, .webform-submission-form', context).each(function() {
        var $external_media_widget_wrapper = $(this).find('.external-media-widget-wrapper');
        if ($external_media_widget_wrapper.length) {
          var $parents = $external_media_widget_wrapper.parent().parent();
          if ($parents.length) {
            $parents.find('input[type=file]').hide();
            $parents.find('input[type=submit]').hide();
            $parents.find('input[name*=remove]').show();
          }
        }
      });

      // Trigger file upload browser
      $('.form-type-external-media a.browse, .webform-submission-form a.browse', context).unbind().click(function(e) {
        var $parent = $(this).closest('.form-managed-file');
        $parent.find('input[type=file]').click();
        e.preventDefault();
      });

      $('.form-type-external-media input.form-file, .webform-submission-form input.form-file', context).change(function() {
        var $parent = $(this).parent().parent();
        if ($parent.find('.external-media-widget-wrapper').length) {
          setTimeout(function() {
            if (!$('.error', $parent).length) {
              $('input.external-media-upload-button', $parent).mousedown();
              $('.button', $parent).unbind().addClass('disabled');
            }
          }, 100);
        }
      });

    }
  };

}(jQuery, Drupal, drupalSettings));
