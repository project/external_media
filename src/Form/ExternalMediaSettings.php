<?php

namespace Drupal\external_media\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\external_media\Plugin\ExternalMediaManager;
use Drupal\external_media\ExternalMedia;

/**
 * ExternalMediaSettings form controller.
 */
class ExternalMediaSettings extends FormBase {

  /**
   * @var \Drupal\external_media\Plugin\ExternalMediaManager
   */
  protected $externalMediaManager;

  /**
   * @var \Drupal\external_media\ExternalMedia
   */
  protected $externalMedia;

  /**
   * Constructs a \Drupal\external_media\Form\ExternalMediaSettings.
   *
   * @param \Drupal\external_media\Plugin\ExternalMediaManager $external_media_manager
   * @param \Drupal\external_media\ExternalMedia $external_media
   */
  public function __construct(ExternalMediaManager $external_media_manager, ExternalMedia $external_media) {
    $this->externalMediaManager = $external_media_manager;
    $this->externalMedia = $external_media;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.external_media'),
      $container->get('external_media')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'external_media_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['settings'] = [
      '#type' => 'vertical_tabs',
      '#attached' => [
        'library' => [
          'external_media/external_media.form',
        ],
      ],
    ];

    foreach ($this->externalMediaManager->getDefinitions() as $plugin) {
      $external_media = $this->externalMediaManager->createInstance($plugin['id']);
      if ($external_media->classExists()) {
        $id = $external_media->getPluginId();
        $form[$id] = [
          '#type' => 'details',
          '#title' => $external_media->getName(),
          '#group' => 'settings',
        ];

        $form[$id][$id . '_description'] = [
          '#markup' => '<div class="em-plugin-description">' . $external_media->getDescription() . '</div>',
        ];

        $form[$id][$id . '_enabled'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable plugin'),
          '#default_value' => $external_media->getSetting('enabled'),
          '#description' => $this->t('Disabled plugins are not visible in <em>File</em> and <em>Image</em> fields.'),
        ];

        $form[$id][$id . '_label'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Button label'),
          '#default_value' => ($label = $external_media->getSetting('button_label')) ? $label : $external_media->getName(),
          '#description' => $this->t('This label appears in the button that triggers the picker.'),
          '#size' => 25,
        ];

        $form = array_merge($form, $external_media->configForm($form, $form_state));

        if ($external_media->setRedirectCallback()) {
          $form[$id][$id . '_redirect_url'] = [
            '#markup' => '<em>' . $external_media->getRedirectUrl()->toString() . '</em>',
            '#prefix' => '<div><strong>' . $this->t('Redirect URL') . '</strong></div>',
            '#suffix' => '<div class="description">' . $this->t('This is the URL you will need for the redirect URL/OAuth authentication') . '</div>',
          ];
        }
      }
    }
    $form['actions'] = [
      '#weight' => 999,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Settings'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->externalMediaManager->getDefinitions() as $plugin) {
      $external_media = $this->externalMediaManager->createInstance($plugin['id']);
      if ($external_media->classExists()) {
        $external_media->setSetting('enabled', $form_state->getValue($external_media->getPluginId() . '_enabled'));
        $external_media->setSetting('button_label', $form_state->getValue($external_media->getPluginId() . '_label'));
        $external_media->submitConfigForm($form, $form_state);
      }
    }
    $this->messenger()->addStatus(t('The configuration options have been saved.'));
  }

}
